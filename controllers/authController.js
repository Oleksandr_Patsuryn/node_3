const User = require('../models/User');
const Credentials = require('../models/Credentials');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');
const fs = require("fs");
const generator = require('generate-password');
const sendEmail = require("../utils/sendMail");

const generateAccessToken = (id, role) => {
    const payload = {id, role};
    return jwt.sign(payload, secret, {expiresIn: "24h"});
}

const roles = [
    "SHIPPER",
    "DRIVER"
]

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                fs.appendFile('logs.log', `Code 400: Registration error.\n`, () => {
                });
                return res.status(400).json({message: "Registration error", errors});
            }
            const {role, email, password} = req.body;
            const candidate = await User.findOne({email});
            if (candidate) {
                fs.appendFile('logs.log', `Code 400: User with such username already exists.\n`, () => {
                });
                return res.status(400).json({message: "User with such username already exists"});
            }
            if (!roles.includes(role.toUpperCase())) {
                fs.appendFile('logs.log', `Code 400: Incorrect role. Choose the right one(shipper/driver)\n`,
                    () => {});
                return res.status(400).json({message: "Incorrect role. Choose the right one(shipper/driver)"});
            }
            const hashPassword = bcrypt.hashSync(password, 10);
            const credentials = await new Credentials({email: `${email}`, password: `${hashPassword}`});
            const user = await new User({role: `${role.toUpperCase()}`, email: `${email}`, createdDate: `${Date.now()}`});
            await credentials.save();
            await user.save();
            fs.appendFile('logs.log', `Code 200: User successfully created.\n`, () => {
            });
            return res.status(200).json({message: "User successfully created"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async login(req, res) {
        try {
            const {email, password} = req.body;
            const credentials = await Credentials.findOne({email});
            const user = await User.findOne({email});
            if (!credentials) {
                fs.appendFile('logs.log', `User ${email} was not found\n`, () => {
                });
                return res.status(400).json({message: `User ${email} was not found`});
            }
            const validPassword = bcrypt.compareSync(password, credentials.password);
            if (!validPassword) {
                fs.appendFile('logs.log', `Code 400: Wrong password.\n`, () => {
                });
                return res.status(400).json({message: "Wrong password"});
            }
            const token = generateAccessToken(user._id, user.role);
            fs.appendFile('logs.log', `Code 200: Login successful.\n`, () => {});
            return res.status(200).json({message: 'success', jwt_token: token});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async sendResetLetter(req, res) {
        try {
            const user = await User.findOne({email: req.body.email});
            const credentials = await Credentials.findOne({email: req.body.email});
            if (!user)
                return res.status(400).send("user with given email doesn't exist");
            console.log(req.url);
            const link = `${req.protocol}://${req.get('host')}${req.originalUrl}/${credentials._id}`;
            await sendEmail(req.body.email, "Reset password", link);
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {});
            return res.status(200).json({message: "Success"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            res.status(500).json({message: 'Server error'});
        }
    }

    async resetPassword(req, res) {
        try {
            const credentials = await Credentials.findOne({_id: req.params.id});
            const newPassword = generator.generate({
                length: 15,
                numbers: true
            });
            credentials.password = bcrypt.hashSync(newPassword, 10);
            await credentials.save();
            return res.status(200).json({message: `Your new password is: ${newPassword}`});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }
}

module.exports = new authController();