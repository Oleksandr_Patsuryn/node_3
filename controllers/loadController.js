const Truck = require('../models/Truck');
const Load = require('../models/Load');
const jwt = require('jsonwebtoken');
const fs = require("fs");
const { ObjectId } = require('mongodb');

const carTypes = {
    SPRINTER: {
        capacity: 12750000,
        payload: 1700
    },
    SMALL_STRAIGHT: {
        capacity: 21250000,
        payload: 2500
    },
    LARGE_STRAIGHT: {
        capacity: 49000000,
        payload: 4000
    }
}

const status = ["NEW", "POSTED", "ASSIGNED", "SHIPPED"];

const state = ["En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"];

class loadController {
    async getLoads(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            switch (decoded.role) {
                case "DRIVER":
                    const driverLoads = await Load.find({assigned_to: decoded.id, state: (state[0] || state[3])});
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", loads: driverLoads});
                case "SHIPPER":
                    const shipperLoads = await Load.find({created_by: decoded.id, status: status[0]});
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", loads: shipperLoads});
            }
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async addLoad(req, res) {
        try {
            const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "SHIPPER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not shipper.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not shipper"});
            } else if (dimensions.width === 0 || dimensions.length === 0 || dimensions.height === 0) {
                fs.appendFile('logs.log', `Code 400: Dimensions can not be equal to zero.\n`, () => {
                });
                return res.status(400).json({message: "Dimensions can not be equal to zero"});
            }
            const load = await new Load({
                created_by: decoded.id,
                status: status[0],
                name: name,
                payload: payload,
                pickup_address: pickup_address,
                delivery_address: delivery_address,
                dimensions: dimensions,
                logs: [{
                    message: `Load created by shipper with id ${decoded.id}`,
                    time: Date.now()
                }],
                created_date: Date.now().toString()
            });
            await load.save();
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", load: load});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async getActiveLoad(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            }
            const load = await Load.find({assigned_to: decoded.id, state: (state[0] || state[1] || state[2])});
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", load: load});

        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async changeState(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            }
            const load = await Load.findOne({assigned_to: decoded.id});
            if (load === null) {
                fs.appendFile('logs.log', "Code 400: You can't change state for non existing load.\n", () => {
                });
                return res.status(400).json({message: "You can't change state for non existing load"});
            }
            const truck = await Truck.findOne({assigned_to: decoded.id});
            switch (load.state) {
                case undefined:
                    load.state = state[0];
                    load.logs.push({
                        message: `Load state changed to '${state[0]}'`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    fs.appendFile('logs.log', `Code 200: Load state changed to '${state[0]}'.\n`, () => {
                    });
                    return res.status(200).json({message: `Load state changed to '${state[0]}'`, load: load});
                case state[0]:
                    load.state = state[1];
                    load.logs.push({
                        message: `Load state changed to '${state[1]}'`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    fs.appendFile('logs.log', `Code 200:Load state changed to '${state[1]}'.\n`, () => {
                    });
                    return res.status(200).json({message: `Load state changed to '${state[1]}'`, load: load});
                case state[1]:
                    load.state = state[2];
                    load.logs.push({
                        message: `Load state changed to '${state[2]}'`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    fs.appendFile('logs.log', `Code 200: Load state changed to '${state[2]}'.\n`, () => {
                    });
                    return res.status(200).json({message: `Load state changed to '${state[2]}'`, load: load});
                case state[2]:
                    load.state = state[3];
                    truck.state = "IS";
                    load.logs.push({
                        message: `Load state changed to '${state[3]}'`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    await truck.save();
                    fs.appendFile('logs.log', `Code 200: Load state changed to '${state[3]}'.\n`, () => {
                    });
                    return res.status(200).json({message: `Load state changed to '${state[3]}'`, load: load});
            }
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async getUserLoads(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            switch (decoded.role) {
                case "DRIVER":
                    const driverLoad = await Load.findOne({_id: id, assigned_to: decoded.id});
                    if (driverLoad === null) {
                        fs.appendFile('logs.log', "Code 400: You can't access to not yours load.\n", () => {
                        });
                        return res.status(400).json({message: "You can't access to not yours load"});
                    }
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", loads: driverLoad});
                case "SHIPPER":
                    const shipperLoad = await Load.find({_id: id, created_by: decoded.id});
                    if (shipperLoad === null) {
                        fs.appendFile('logs.log', "Code 400: You can't access to not yours load.\n", () => {
                        });
                        return res.status(400).json({message: "You can't access to not yours load"});
                    }
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", loads: shipperLoad});
            }
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async updateLoad(req, res) {
        try {
            const {id} = req.params;
            const data = req.body;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "SHIPPER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not shipper.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not shipper"});
            } else if (data.assigned_to || data.created_by || data.status || data.logs || data.state) {
                fs.appendFile('logs.log', `Code 400: Forbidden. You can't change these fields.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You can't change these fields"});
            }
            const load = await Load.findOneAndUpdate({_id: id}, data, {
                new: true
            });
            load.logs.push({
                message: "Load data was updated",
                time: Date.now().toString()
            });
            await load.save();
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async deleteLoad(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "SHIPPER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not shipper.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not shipper"});
            }
            const load = await Load.findOne({_id: id});
            if (load === null) {
                fs.appendFile('logs.log', `Code 400: There are no loads with this id.\n`, () => {
                });
                return res.status(400).json({message: "There are no loads with this id"});
            }
            const truck = await Truck.findOne({assigned_to: load.assigned_to});
            truck.status = "IS";
            await truck.save();
            load.deleteOne();
            fs.appendFile('logs.log', `Code 200: Truck deleted successfully.\n`, () => {
            });
            return res.status(200).json({message: "Truck deleted successfully"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async postLoad(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            let driver_found = false;
            if (decoded.role !== "SHIPPER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not shipper.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not shipper"});
            }
            const load = await Load.findOne({_id: id});
            if (load === null) {
                fs.appendFile('logs.log', `Code 400: There are no loads with this id.\n`, () => {
                });
                return res.status(400).json({message: "There are no loads with this id"});
            } else if (load.status !== "NEW") {
                fs.appendFile('logs.log', `Code 400: You can post only new loads.\n`, () => {
                });
                return res.status(400).json({message: "You can't post only new loads"});
            } else if (load.created_by !== decoded.id) {
                fs.appendFile('logs.log', `Code 400: You can post only your loads.\n`, () => {
                });
                return res.status(400).json({message: "You can post only your loads"});
            }
            load.status = "POSTED";
            const loadDimensions = load.dimensions.length * load.dimensions.width * load.dimensions.height;
            switch (true) {
                case (loadDimensions <= carTypes.SPRINTER.capacity) || (load.payload <= carTypes.SPRINTER.payload):
                    const smallTruck = await Truck.findOne({
                        status: "IS",
                        assigned_to: {$exists: true},
                        type: "SPRINTER"
                    });
                    if (smallTruck === null) {
                        load.status = "NEW";
                        await load.save();
                        fs.appendFile('logs.log', `Code 400: There are no available trucks now.\n`, () => {
                        });
                        return res.status(400).json({message: "There are no available trucks now"});
                    }
                    smallTruck.status = "OL";
                    load.assigned_to = smallTruck.assigned_to;
                    load.status = status[2];
                    load.state = state[0];
                    load.logs.push({
                        message: `Load was assigned to driver with id ${smallTruck.assigned_to}`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    await smallTruck.save();
                    driver_found = true;
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", driver_found: driver_found});
                case carTypes.SPRINTER.capacity < loadDimensions <= carTypes.SMALL_STRAIGHT.capacity || load.payload <= carTypes.SPRINTER.payload:
                    const mediumTruck = await Truck.findOne({
                        status: "IS",
                        assigned_to: {$exists: true},
                        type: "SMALL_STRAIGHT"
                    });
                    if (mediumTruck === null) {
                        load.status = "NEW";
                        await load.save();
                        fs.appendFile('logs.log', `Code 400: There are no available trucks now.\n`, () => {
                        });
                        return res.status(400).json({message: "There are no available trucks now"});
                    }
                    mediumTruck.status = "OL";
                    load.assigned_to = mediumTruck.assigned_to;
                    load.status = status[2];
                    load.state = state[0];
                    load.logs.push({
                        message: `Load was assigned to driver with id ${mediumTruck.assigned_to}`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    await mediumTruck.save();
                    driver_found = true;
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", driver_found: driver_found});
                case carTypes.SMALL_STRAIGHT.capacity < loadDimensions <= carTypes.LARGE_STRAIGHT.capacity || load.payload <= carTypes.SPRINTER.payload:
                    const largeTruck = await Truck.findOne({
                        status: "IS",
                        assigned_to: {$exists: true},
                        type: "LARGE_STRAIGHT"
                    });
                    if (largeTruck === null) {
                        load.status = "NEW";
                        await load.save();
                        fs.appendFile('logs.log', `Code 400: There are no available trucks now.\n`, () => {
                        });
                        return res.status(400).json({message: "There are no available trucks now"});
                    }
                    largeTruck.status = "OL";
                    load.assigned_to = largeTruck.assigned_to;
                    load.status = status[2];
                    load.state = state[0];
                    load.logs.push({
                        message: `Load was assigned to driver with id ${largeTruck.assigned_to}`,
                        time: Date.now().toString()
                    });
                    await load.save();
                    await largeTruck.save();
                    driver_found = true;
                    fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
                    });
                    return res.status(200).json({message: "Success", driver_found: driver_found});
                default:
                    load.status = "NEW";
                    await load.save();
                    fs.appendFile('logs.log', `Code 400: There are no available trucks now.\n`, () => {
                    });
                    return res.status(400).json({message: "There are no available trucks now"});
            }
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async getShippingInfo(req, res) {
        try {
            const {id} = req.params;
            const load_id = ObjectId(id);
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "SHIPPER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not shipper.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not shipper"});
            }
            const load = await Load.findOne({_id: load_id, created_by: decoded.id});
            if (load === null) {
                fs.appendFile('logs.log', `Code 400: You can check only your loads.\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You can check only your loads"});
            }
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", load: load});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }
}

module.exports = new loadController();
