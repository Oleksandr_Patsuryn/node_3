const Truck = require('../models/Truck');
const jwt = require('jsonwebtoken');
const fs = require("fs");

const types = ["SPRINTER", "SMALL_STRAIGHT", "LARGE_STRAIGHT"];

const status = ["OL", "IS"];

class truckController {
    async getTrucks(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const trucks = await Truck.find({created_by: decoded.id});
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", trucks: trucks});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async addTruck(req, res) {
        try {
            const {type} = req.body;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            } else if (type === undefined) {
                fs.appendFile('logs.log', `Code 400: Missing the type field.\n`, () => {
                });
                return res.status(400).json({message: "Missing the type field"});
            } else if (!types.includes(type.toUpperCase())) {
                fs.appendFile('logs.log', `Code 400: Wrong type\n`, () => {
                });
                return res.status(400).json({message: "Wrong type"});
            }
            const truck = await new Truck({
                created_by: decoded.id,
                type: type.toUpperCase(),
                status: status[1],
                created_date: Date.now()
            });
            await truck.save();
            fs.appendFile('logs.log', `Code 200: Truck created successfully.\n`, () => {
            });
            return res.status(200).json({message: "Truck created successfully"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async getTruckById(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            }
            const truck = await Truck.findOne({_id: id});
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", truck: truck});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async updateTruck(req, res) {
        try {
            const {id} = req.params;
            const {type} = req.body;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            } else if (type === undefined) {
                fs.appendFile('logs.log', `Code 400: Missing the type field.\n`, () => {
                });
                return res.status(400).json({message: "Missing the type field"});
            }
            const truck = await Truck.findOne({_id: id});
            if (!types.includes(type.toUpperCase()) || type.toUpperCase() === truck.type) {
                fs.appendFile('logs.log', `Code 400: Wrong type\n`, () => {
                });
                return res.status(400).json({message: "Wrong type"});
            } else if (truck === null) {
                fs.appendFile('logs.log', `Code 400: There are no trucks with this id.\n`, () => {
                });
                return res.status(400).json({message: "There are no trucks with this id"});
            } else if (truck.status === "OL") {
                fs.appendFile('logs.log', `Code 400: You can't update truck while on load.\n`, () => {
                });
                return res.status(400).json({message: "You can't update truck while on load"});
            }
            truck.type = type.toUpperCase();
            await truck.save();
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", trucks: truck});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async deleteTruck(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            }
            const truck = await Truck.findOne({_id: id});
            if (truck === null) {
                fs.appendFile('logs.log', `Code 400: There are no trucks with this id.\n`, () => {
                });
                return res.status(400).json({message: "There are no trucks with this id"});
            } else if (truck.status === "OL") {
                fs.appendFile('logs.log', `Code 400: You can't update truck while on load.\n`, () => {
                });
                return res.status(400).json({message: "You can't update truck while on load"});
            }
            truck.deleteOne();
            fs.appendFile('logs.log', `Code 200: Truck deleted successfully.\n`, () => {
            });
            return res.status(200).json({message: "Truck deleted successfully"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async assignTruck(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: Forbidden. You are not driver\n`, () => {
                });
                return res.status(400).json({message: "Forbidden. You are not driver"});
            }
            const oldTruck = await Truck.findOne({assigned_to: decoded.id})
            const truck = await Truck.findOne({_id: id});
            if (truck === null) {
                fs.appendFile('logs.log', `Code 400: There are no trucks with this id.\n`, () => {
                });
                return res.status(400).json({message: "There are no trucks with this id"});
            } else if (truck.status === "OL") {
                fs.appendFile('logs.log', `Code 400: You can't update truck while on load.\n`, () => {
                });
                return res.status(400).json({message: "You can't update truck while on load"});
            } else if (oldTruck._id.toString() === id) {
                fs.appendFile('logs.log', `Code 400: You are assigned to this car already.\n`, () => {
                });
                return res.status(400).json({message: "You are assigned to this car already"});
            }
            truck.assigned_to = decoded.id;
            oldTruck.assigned_to = undefined;
            await truck.save();
            await oldTruck.save();
            fs.appendFile('logs.log', `Code 200: Truck assigned successfully.\n`, () => {
            });
            return res.status(200).json({message: "Truck assigned successfully"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }
}

module.exports = new truckController();