const User = require('../models/User');
const Credentials = require('../models/Credentials');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const fs = require("fs");

class userController {
    async getUser(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const user = await User.findById(decoded.id);
            fs.appendFile('logs.log', `Code 200: User info received successfully.\n`, () => {});
            return res.status(200).json(user);
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            res.status(500).json({message: 'Server error'});
        }
    }

    async deleteUser(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const user = await User.findById(decoded.id);
            await Credentials.find({email: user.email}).deleteOne();
            await user.deleteOne();
            fs.appendFile('logs.log', `Code 200: User was successfully deleted.\n`, () => {});
            return res.status(200).json({message: "User was successfully deleted"});
        } catch (e) {
            console.log(e);
            res.status(500).json({message: 'Server error'});
        }
    }

    async updateUser(req, res) {
        try {
            const {oldPassword, newPassword} = req.body;
            let token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const user = await User.findById(decoded.id);
            const cred = await Credentials.findOne({email: user.email});
            const validPassword = bcrypt.compareSync(oldPassword, cred.password);
            if (!validPassword) {
                fs.appendFile('logs.log', `Code 400: Wrong password.\n`, () => {});
                return res.status(400).json({message: "Wrong password"});
            }
            cred.password = bcrypt.hashSync(newPassword, 10);
            await cred.save();
            fs.appendFile('logs.log', `Code 200: Password was successfully changed.\n`, () => {});
            return res.status(200).json({message: "Password was successfully changed", jwt_token: token});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            res.status(500).json({message: 'Server error'});
        }
    }
}

module.exports = new userController();