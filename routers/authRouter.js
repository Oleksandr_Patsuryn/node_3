const Router = require('express');
const router = new Router();
const controller = require('../controllers/authController');
const joi = require('joi');
const validator = require('express-joi-validation').createValidator({});

const querySchema = joi.object({
    email: joi.string(),
    password: joi.string().min(4).max(16)
})

router.post('/register', validator.query(querySchema), controller.registration);
router.post('/login', controller.login);
router.post('/forgot_password', controller.sendResetLetter);
router.get('/forgot_password/:id', controller.resetPassword);


module.exports = router;